(* ----------------------------------------------------------------------------- *) 
(* This model file was automatically created by SARAH version4.14.3  *) 
(* SARAH References: arXiv:0806.0538, 0909.2863, 1002.0840, 1207.0906, 1309.7223 *) 
(* (c) Florian Staub, 2013  *) 
(* ----------------------------------------------------------------------------- *) 
(* File created at 18:57 on 25.10.2021  *) 
(* ---------------------------------------------------------------------- *) 
 
 
IndexRange[  Index[Colour]  ] =NoUnfold[Range[3]]; 
IndexStyle[  Index[Colour, i_Integer ] ] := Greek[i];  
IndexRange[  Index[I2Gen]  ] =Range[2]; 
IndexStyle[  Index[I2Gen, i_Integer ] ] := Alph[ 8+i];  
IndexRange[  Index[I3Gen]  ] =Range[3]; 
IndexStyle[  Index[I3Gen, i_Integer ] ] := Alph[ 8+i];  
IndexRange[  Index[Gluon]  ] =NoUnfold[Range[8]]; 
IndexStyle[  Index[Gluon, i_Integer ] ] := Alph[ 8+i];  

 
(* Definitions for trigonometric functions  
Sin[ThetaW]: STW
Sin[2*ThetaW]: S2TW
Sin[TW]: STW
Sin[2*TW]: S2TW
Cos[ThetaW]: CTW
Cos[2*ThetaW]: C2TW
Cos[TW]: CTW
Cos[2*TW]: C2TW
*) 
 
Conjugate[STW] ^= STW
Conjugate[S2TW] ^= S2TW
Conjugate[STW] ^= STW
Conjugate[S2TW] ^= S2TW
Conjugate[CTW] ^= CTW
Conjugate[C2TW] ^= C2TW
Conjugate[CTW] ^= CTW
Conjugate[C2TW] ^= C2TW
 
 
Lam[a_,b_,c_]:=2*SUNT[a,b,c]; 
fSU3[a_,b_,c_]:=SUNF[a,b,c]; 
LambdaProd[a_,b_][c_,d_]:=4*SUNT[a,b,c,d]; 
 
 
M$ClassesDescription= {
S[4] == {SelfConjugate -> True,
Indices -> {},
Mass -> MasssigmaS,
PropagatorLabel->ComposedChar["\\sigma","S"],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[1] == {SelfConjugate -> True,
Indices -> {Index[I3Gen]},
Mass -> Masshh,
PropagatorLabel->ComposedChar["h",Index[I3Gen]],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[2] == {SelfConjugate -> True,
Indices -> {Index[I2Gen]},
Mass -> MassAh,
PropagatorLabel->ComposedChar["A",Index[I2Gen],"0"],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[3] == {SelfConjugate -> False,
Indices -> {Index[I2Gen]},
Mass -> MassHm,
PropagatorLabel->ComposedChar["H",Index[I2Gen],"-"],
PropagatorType -> ScalarDash,
PropagatorArrow -> Forward},

 
F[1] == {SelfConjugate -> False,
Indices -> {Index[I3Gen]},
Mass -> MassFv,
PropagatorLabel->ComposedChar["\\nu",Index[I3Gen]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[4] == {SelfConjugate -> False,
Indices -> {Index[I3Gen], Index[Colour]},
Mass -> MassFd,
PropagatorLabel->ComposedChar["d",Index[I3Gen],Index[Colour]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[3] == {SelfConjugate -> False,
Indices -> {Index[I3Gen], Index[Colour]},
Mass -> MassFu,
PropagatorLabel->ComposedChar["u",Index[I3Gen],Index[Colour]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[2] == {SelfConjugate -> False,
Indices -> {Index[I3Gen]},
Mass -> MassFe,
PropagatorLabel->ComposedChar["e",Index[I3Gen]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
V[5] == {SelfConjugate -> True,
Indices -> {Index[Gluon]},
Mass -> 0,
PropagatorLabel->ComposedChar["g",Index[Gluon]],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[1] == {SelfConjugate -> True,
Indices -> {},
Mass -> 0,
PropagatorLabel->ComposedChar["\\gamma"],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[2] == {SelfConjugate -> True,
Indices -> {},
Mass -> MassVZ,
PropagatorLabel->ComposedChar["Z"],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[3] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWm,
PropagatorLabel->ComposedChar["W","-"],
PropagatorType -> Sine,
PropagatorArrow -> Forward},

 
U[5] == {SelfConjugate -> False,
Indices -> {Index[Gluon]},
Mass -> 0,
PropagatorLabel->ComposedChar["\\eta",Index[Gluon],"G"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[1] == {SelfConjugate -> False,
Indices -> {},
Mass -> 0,
PropagatorLabel->ComposedChar["\\eta","\\gamma"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[2] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVZ,
PropagatorLabel->ComposedChar["\\eta","Z"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[3] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWm,
PropagatorLabel->ComposedChar["\\eta","-"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[4] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWm,
PropagatorLabel->ComposedChar["\\eta","+"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward}
 
}

 
MassFd[gen_, y_] = MassFd[gen]
MassFu[gen_, y_] = MassFu[gen]


GaugeXi[S[4,___]] = 1 
GaugeXi[S[1,___]] = 1 
GaugeXi[S[2,{a_Integer}]] = 1 /; a > 1 
GaugeXi[S[2,1]] = GaugeXi[VZ] /; a > 1 
GaugeXi[S[3,{a_Integer}]] = 1 /; a > 1 
GaugeXi[S[3,1]] = GaugeXi[VWm] /; a > 1 


GaugeXi[V[5,___]] = GaugeXi[G]
GaugeXi[V[1,___]] = GaugeXi[P]
GaugeXi[V[2,___]] = GaugeXi[Z]
GaugeXi[V[3,___]] = GaugeXi[Wm]


M$CouplingMatrices= {
C[V[5, {ct1}], V[5, {ct2}], V[5, {ct3}], V[5, {ct4}]] == {{(-I)*g3^2*IndexSum[fSU3[ct1, ct4, j1]*fSU3[ct2, ct3, j1], {j1, 8}] - I*g3^2*IndexSum[fSU3[ct1, ct3, j1]*fSU3[ct2, ct4, j1], {j1, 8}]}, {I*g3^2*IndexSum[fSU3[ct1, ct4, j1]*fSU3[ct2, ct3, j1], {j1, 8}] - I*g3^2*IndexSum[fSU3[ct1, ct2, j1]*fSU3[ct3, ct4, j1], {j1, 8}]}, {I*g3^2*IndexSum[fSU3[ct1, ct3, j1]*fSU3[ct2, ct4, j1], {j1, 8}] + I*g3^2*IndexSum[fSU3[ct1, ct2, j1]*fSU3[ct3, ct4, j1], {j1, 8}]}},
 C[-V[3], V[1], V[1], V[3]] == {{I*g2^2*STW^2}, {I*g2^2*STW^2}, {(-2*I)*g2^2*STW^2}},
 C[-V[3], V[1], V[3], V[2]] == {{I*CTW*g2^2*STW}, {(-2*I)*CTW*g2^2*STW}, {I*CTW*g2^2*STW}},
 C[-V[3], -V[3], V[3], V[3]] == {{(2*I)*g2^2}, {(-I)*g2^2}, {(-I)*g2^2}},
 C[-V[3], V[3], V[2], V[2]] == {{(-2*I)*CTW^2*g2^2}, {I*CTW^2*g2^2}, {I*CTW^2*g2^2}},
 C[S[2, {gt1}], S[2, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/2)*g2^2*ZA[gt1, 2]*ZA[gt2, 2]}},
 C[S[2, {gt1}], S[2, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZA[gt1, 1]*ZA[gt2, 1] - (I/4)*CTW^2*g1^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*CTW^2*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + I*CTW*g1*g2*STW*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g1^2*STW^2*ZA[gt1, 1]*ZA[gt2, 1] - (I/4)*g2^2*STW^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g1^2*ZA[gt1, 2]*ZA[gt2, 2] - (I/4)*CTW^2*g1^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*g2^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*CTW^2*g2^2*ZA[gt1, 2]*ZA[gt2, 2] + I*CTW*g1*g2*STW*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*g1^2*STW^2*ZA[gt1, 2]*ZA[gt2, 2] - (I/4)*g2^2*STW^2*ZA[gt1, 2]*ZA[gt2, 2]}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3], V[1]] == {{-(CTW*g1*g2*ZA[gt1, 1]*ZP[gt2, 1])/2 - (CTW*g1*g2*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3], V[2]] == {{(g1*g2*STW*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g1*g2*STW*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[1], V[3]] == {{(CTW*g1*g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (CTW*g1*g2*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[3], V[2]] == {{-(g1*g2*STW*ZA[gt1, 1]*ZP[gt2, 1])/2 - (g1*g2*STW*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[1, {gt1}], S[1, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/2)*g2^2*ZH[gt1, 2]*ZH[gt2, 2]}},
 C[S[1, {gt1}], S[1, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZH[gt1, 1]*ZH[gt2, 1] - (I/4)*CTW^2*g1^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*CTW^2*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + I*CTW*g1*g2*STW*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g1^2*STW^2*ZH[gt1, 1]*ZH[gt2, 1] - (I/4)*g2^2*STW^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g1^2*ZH[gt1, 2]*ZH[gt2, 2] - (I/4)*CTW^2*g1^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*g2^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*CTW^2*g2^2*ZH[gt1, 2]*ZH[gt2, 2] + I*CTW*g1*g2*STW*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*g1^2*STW^2*ZH[gt1, 2]*ZH[gt2, 2] - (I/4)*g2^2*STW^2*ZH[gt1, 2]*ZH[gt2, 2]}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3], V[1]] == {{(I/2)*CTW*g1*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g1*g2*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3], V[2]] == {{(-I/2)*g1*g2*STW*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[1], V[3]] == {{(I/2)*CTW*g1*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g1*g2*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[3], V[2]] == {{(-I/2)*g1*g2*STW*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1], V[1]] == {{(I/4)*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*CTW^2*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*CTW^2*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + I*CTW*g1*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*g1^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*CTW^2*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*CTW^2*g2^2*ZP[gt1, 2]*ZP[gt2, 2] + I*CTW*g1*g2*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*g1^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1], V[2]] == {{(I/2)*CTW^2*g1*g2*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*CTW*g1^2*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g2^2*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW^2*g1*g2*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*CTW*g1^2*STW*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*CTW*g2^2*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*g1*g2*STW^2*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*g2^2*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*CTW^2*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*CTW^2*g2^2*ZP[gt1, 1]*ZP[gt2, 1] - I*CTW*g1*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*g2^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*CTW^2*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*CTW^2*g2^2*ZP[gt1, 2]*ZP[gt2, 2] - I*CTW*g1*g2*STW*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g1^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*g2^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], S[2, {gt3}]] == {{-((IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 1])/Sqrt[2])}, {(IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZA[gt3, 1])/Sqrt[2]}},
 C[-F[2, {gt1}], F[2, {gt2}], S[2, {gt3}]] == {{-((IndexSum[Conjugate[ZEL[gt2, j2]]*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 1])/Sqrt[2])}, {(IndexSum[IndexSum[Conjugate[Ye[j1, j2]]*ZER[gt2, j1], {j1, 3}]*ZEL[gt1, j2], {j2, 3}]*ZA[gt3, 1])/Sqrt[2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], S[2, {gt3}]] == {{(IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 2])/Sqrt[2]}, {-((IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZA[gt3, 2])/Sqrt[2])}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], S[1, {gt3}]] == {{((-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}, {((-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}},
 C[-F[3, {gt1, ct1}], F[4, {gt2, ct2}], -S[3, {gt3}]] == {{I*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZP[gt3, 2]}, {(-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZP[gt3, 1]}},
 C[-F[2, {gt1}], F[2, {gt2}], S[1, {gt3}]] == {{((-I)*IndexSum[Conjugate[ZEL[gt2, j2]]*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}, {((-I)*IndexSum[IndexSum[Conjugate[Ye[j1, j2]]*ZER[gt2, j1], {j1, 3}]*ZEL[gt1, j2], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}},
 C[-F[1, {gt1}], F[2, {gt2}], -S[3, {gt3}]] == {{0}, {(-I)*IndexSum[Conjugate[Ye[j1, gt1]]*ZER[gt2, j1], {j1, 3}]*ZP[gt3, 1]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], S[1, {gt3}]] == {{((-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 2])/Sqrt[2]}, {((-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZH[gt3, 2])/Sqrt[2]}},
 C[-F[4, {gt1, ct1}], F[3, {gt2, ct2}], S[3, {gt3}]] == {{(-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZP[gt3, 1]}, {I*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZP[gt3, 2]}},
 C[-F[2, {gt1}], F[1, {gt2}], S[3, {gt3}]] == {{(-I)*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, gt2], {j1, 3}]*ZP[gt3, 1]}, {0}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[5, {ct3}]] == {{(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}, {(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[1]] == {{(-I/6)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/2)*g2*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {(I/3)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[2]] == {{(I/2)*CTW*g2*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/6)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {(-I/3)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[3, {gt1, ct1}], F[4, {gt2, ct2}], -V[3]] == {{((-I)*g2*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j1]]*ZUL[gt1, j1], {j1, 3}])/Sqrt[2]}, {0}},
 C[-F[2, {gt1}], F[2, {gt2}], V[1]] == {{(I/2)*CTW*g1*IndexDelta[gt1, gt2] + (I/2)*g2*STW*IndexDelta[gt1, gt2]}, {I*CTW*g1*IndexDelta[gt1, gt2]}},
 C[-F[2, {gt1}], F[2, {gt2}], V[2]] == {{(I/2)*CTW*g2*IndexDelta[gt1, gt2] - (I/2)*g1*STW*IndexDelta[gt1, gt2]}, {(-I)*g1*STW*IndexDelta[gt1, gt2]}},
 C[-F[1, {gt1}], F[2, {gt2}], -V[3]] == {{((-I)*g2*Conjugate[ZEL[gt2, gt1]])/Sqrt[2]}, {0}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[5, {ct3}]] == {{(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}, {(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[1]] == {{(-I/6)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] - (I/2)*g2*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {((-2*I)/3)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[4, {gt1, ct1}], F[3, {gt2, ct2}], V[3]] == {{((-I)*g2*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j1]]*ZDL[gt1, j1], {j1, 3}])/Sqrt[2]}, {0}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[2]] == {{(-I/2)*CTW*g2*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/6)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {((2*I)/3)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[2, {gt1}], F[1, {gt2}], V[3]] == {{((-I)*g2*ZEL[gt1, gt2])/Sqrt[2]}, {0}},
 C[-F[1, {gt1}], F[1, {gt2}], V[2]] == {{(-I/2)*CTW*g2*IndexDelta[gt1, gt2] - (I/2)*g1*STW*IndexDelta[gt1, gt2]}, {0}},
 C[S[2, {gt1}], S[2, {gt2}], S[1, {gt3}]] == {{(-I)*Lam1*vd*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 1] - I*Lam5*vu*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 1] - I*Lam5*vu*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 1] - I*Lam3*vd*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] - I*Lam4*vd*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] + I*Lam5*vd*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] - I*Lam3*vu*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] - I*Lam4*vu*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] + I*Lam5*vu*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] - I*Lam5*vd*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2] - I*Lam5*vd*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2] - I*Lam2*vu*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2] - I*Lam7*vS*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3] - I*Lam8*vS*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]}},
 C[S[2, {gt1}], S[3, {gt2}], -S[3, {gt3}]] == {{-(Lam4*vu*ZA[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1])/2 + (Lam5*vu*ZA[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1])/2 + (Lam4*vd*ZA[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1])/2 - (Lam5*vd*ZA[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1])/2 + (Lam4*vu*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2])/2 - (Lam5*vu*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2])/2 - (Lam4*vd*ZA[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2])/2 + (Lam5*vd*ZA[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2])/2}},
 C[S[1, {gt1}], S[1, {gt2}], S[1, {gt3}]] == {{(-3*I)*Lam1*vd*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 1] - I*Lam3*vu*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - I*Lam4*vu*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - I*Lam5*vu*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - I*Lam7*vS*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1] - I*Lam3*vu*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam4*vu*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam5*vu*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam3*vd*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam4*vd*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam5*vd*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - I*Lam7*vS*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1] - I*Lam7*vd*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1] - I*Lam3*vu*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam4*vu*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam5*vu*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam3*vd*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam4*vd*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam5*vd*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - I*Lam3*vd*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - I*Lam4*vd*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - I*Lam5*vd*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - (3*I)*Lam2*vu*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2] - I*Lam8*vS*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2] - I*Lam8*vS*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2] - I*Lam8*vu*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2] - I*Lam7*vS*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3] - I*Lam7*vd*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3] - I*Lam8*vS*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3] - I*Lam8*vu*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3] - I*Lam7*vd*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3] - I*Lam8*vu*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3] - (3*I)*Lam6*vS*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]}},
 C[S[1, {gt1}], S[3, {gt2}], -S[3, {gt3}]] == {{(-I)*Lam1*vd*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 1] - I*Lam3*vu*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1] - I*Lam7*vS*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 1] - (I/2)*Lam4*vu*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*Lam5*vu*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*Lam4*vd*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*Lam5*vd*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*Lam4*vu*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2] - (I/2)*Lam5*vu*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2] - (I/2)*Lam4*vd*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - (I/2)*Lam5*vd*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - I*Lam3*vd*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2] - I*Lam2*vu*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2] - I*Lam8*vS*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2]}},
 C[S[1, {gt1}], S[4], S[4]] == {{(-I)*Lam7*vd*ZH[gt1, 1] - I*Lam8*vu*ZH[gt1, 2] - I*Lam6*vS*ZH[gt1, 3]}},
 C[S[2, {gt1}], S[1, {gt2}], V[2]] == {{-(CTW*g2*ZA[gt1, 1]*ZH[gt2, 1])/2 - (g1*STW*ZA[gt1, 1]*ZH[gt2, 1])/2 - (CTW*g2*ZA[gt1, 2]*ZH[gt2, 2])/2 - (g1*STW*ZA[gt1, 2]*ZH[gt2, 2])/2}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3]] == {{(g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g2*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[3]] == {{(g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g2*ZA[gt1, 2]*ZP[gt2, 2])/2}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3]] == {{(-I/2)*g2*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g2*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[3]] == {{(I/2)*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*g2*ZH[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1]] == {{(I/2)*CTW*g1*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g1*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*g2*STW*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[2]] == {{(I/2)*CTW*g2*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g2*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*g1*STW*ZP[gt1, 2]*ZP[gt2, 2]}},
 C[S[1, {gt1}], -V[3], V[3]] == {{(I/2)*g2^2*vd*ZH[gt1, 1] + (I/2)*g2^2*vu*ZH[gt1, 2]}},
 C[S[1, {gt1}], V[2], V[2]] == {{(I/4)*g1^2*vd*ZH[gt1, 1] - (I/4)*CTW^2*g1^2*vd*ZH[gt1, 1] + (I/4)*g2^2*vd*ZH[gt1, 1] + (I/4)*CTW^2*g2^2*vd*ZH[gt1, 1] + I*CTW*g1*g2*STW*vd*ZH[gt1, 1] + (I/4)*g1^2*STW^2*vd*ZH[gt1, 1] - (I/4)*g2^2*STW^2*vd*ZH[gt1, 1] + (I/4)*g1^2*vu*ZH[gt1, 2] - (I/4)*CTW^2*g1^2*vu*ZH[gt1, 2] + (I/4)*g2^2*vu*ZH[gt1, 2] + (I/4)*CTW^2*g2^2*vu*ZH[gt1, 2] + I*CTW*g1*g2*STW*vu*ZH[gt1, 2] + (I/4)*g1^2*STW^2*vu*ZH[gt1, 2] - (I/4)*g2^2*STW^2*vu*ZH[gt1, 2]}},
 C[S[3, {gt1}], -V[3], V[1]] == {{(I/2)*CTW*g1*g2*vd*ZP[gt1, 1] + (I/2)*CTW*g1*g2*vu*ZP[gt1, 2]}},
 C[S[3, {gt1}], -V[3], V[2]] == {{(-I/2)*g1*g2*STW*vd*ZP[gt1, 1] - (I/2)*g1*g2*STW*vu*ZP[gt1, 2]}},
 C[-S[3, {gt1}], V[1], V[3]] == {{(I/2)*CTW*g1*g2*vd*ZP[gt1, 1] + (I/2)*CTW*g1*g2*vu*ZP[gt1, 2]}},
 C[-S[3, {gt1}], V[3], V[2]] == {{(-I/2)*g1*g2*STW*vd*ZP[gt1, 1] - (I/2)*g1*g2*STW*vu*ZP[gt1, 2]}},
 C[S[2, {gt1}], S[2, {gt2}], S[2, {gt3}], S[2, {gt4}]] == {{(-3*I)*Lam1*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 1] - I*Lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - I*Lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - I*Lam5*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - I*Lam3*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam3*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - I*Lam3*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam3*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - I*Lam3*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - I*Lam4*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - I*Lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - (3*I)*Lam2*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 2]}},
 C[S[2, {gt1}], S[2, {gt2}], S[1, {gt3}], S[1, {gt4}]] == {{(-I)*Lam1*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] + I*Lam5*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam3*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam4*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] + I*Lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam7*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - I*Lam8*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{(-I)*Lam1*ZA[gt1, 1]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - I*Lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - (I/2)*Lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*Lam3*ZA[gt1, 1]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - I*Lam2*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2]}},
 C[S[2, {gt1}], S[2, {gt2}], S[4], S[4]] == {{(-I)*Lam7*ZA[gt1, 1]*ZA[gt2, 1] - I*Lam8*ZA[gt1, 2]*ZA[gt2, 2]}},
 C[S[2, {gt1}], S[1, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{(Lam4*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1])/2 - (Lam5*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1])/2 - (Lam4*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1])/2 + (Lam5*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1])/2 - (Lam4*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2])/2 + (Lam5*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2])/2 + (Lam4*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2])/2 - (Lam5*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2])/2}},
 C[S[1, {gt1}], S[1, {gt2}], S[1, {gt3}], S[1, {gt4}]] == {{(-3*I)*Lam1*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam4*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam5*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam7*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - I*Lam3*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam3*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*Lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 1] - I*Lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] - I*Lam3*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam3*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*Lam3*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam4*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam5*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - (3*I)*Lam2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam8*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - I*Lam8*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - I*Lam8*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 2] - I*Lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 3] - I*Lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] - I*Lam8*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - I*Lam8*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 3] - I*Lam7*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - I*Lam8*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] - (3*I)*Lam6*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{(-I)*Lam1*ZH[gt1, 1]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - I*Lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - I*Lam7*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 1] - (I/2)*Lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*Lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*Lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*Lam3*ZH[gt1, 1]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - I*Lam2*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - I*Lam8*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2]}},
 C[S[1, {gt1}], S[1, {gt2}], S[4], S[4]] == {{(-I)*Lam7*ZH[gt1, 1]*ZH[gt2, 1] - I*Lam8*ZH[gt1, 2]*ZH[gt2, 2] - I*Lam6*ZH[gt1, 3]*ZH[gt2, 3]}},
 C[S[3, {gt1}], S[3, {gt2}], -S[3, {gt3}], -S[3, {gt4}]] == {{(-2*I)*Lam1*ZP[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - (2*I)*Lam5*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - I*Lam3*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*Lam4*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*Lam3*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*Lam4*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*Lam3*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*Lam4*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*Lam3*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*Lam4*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - (2*I)*Lam5*ZP[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*Lam2*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2]}},
 C[S[3, {gt1}], S[4], S[4], -S[3, {gt4}]] == {{(-I)*Lam7*ZP[gt1, 1]*ZP[gt4, 1] - I*Lam8*ZP[gt1, 2]*ZP[gt4, 2]}},
 C[S[4], S[4], S[4], S[4]] == {{(-3*I)*Lam6}},
 C[V[5, {ct1}], V[5, {ct2}], V[5, {ct3}]] == {{g3*fSU3[ct1, ct2, ct3]}},
 C[-V[3], V[1], V[3]] == {{I*g2*STW}},
 C[-V[3], V[3], V[2]] == {{(-I)*CTW*g2}},
 C[S[2, {gt3}], -U[3], U[3]] == {{-(g2^2*vd*GaugeXi[Wm]*ZA[gt3, 1])/4 - (g2^2*vu*GaugeXi[Wm]*ZA[gt3, 2])/4}},
 C[S[2, {gt3}], -U[4], U[4]] == {{(g2^2*vd*GaugeXi[Wm]*ZA[gt3, 1])/4 + (g2^2*vu*GaugeXi[Wm]*ZA[gt3, 2])/4}},
 C[S[1, {gt3}], -U[2], U[1]] == {{(I/4)*CTW^2*g1*g2*vd*GaugeXi[Z]*ZH[gt3, 1] + (I/4)*CTW*g1^2*STW*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/4)*CTW*g2^2*STW*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/4)*g1*g2*STW^2*vd*GaugeXi[Z]*ZH[gt3, 1] + (I/4)*CTW^2*g1*g2*vu*GaugeXi[Z]*ZH[gt3, 2] + (I/4)*CTW*g1^2*STW*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/4)*CTW*g2^2*STW*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/4)*g1*g2*STW^2*vu*GaugeXi[Z]*ZH[gt3, 2]}},
 C[S[3, {gt3}], -U[3], U[1]] == {{(-I/4)*CTW*g1*g2*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*g2^2*STW*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*CTW*g1*g2*vu*GaugeXi[Wm]*ZP[gt3, 2] - (I/4)*g2^2*STW*vu*GaugeXi[Wm]*ZP[gt3, 2]}},
 C[-S[3, {gt3}], -U[4], U[1]] == {{(-I/4)*CTW*g1*g2*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*g2^2*STW*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*CTW*g1*g2*vu*GaugeXi[Wm]*ZP[gt3, 2] - (I/4)*g2^2*STW*vu*GaugeXi[Wm]*ZP[gt3, 2]}},
 C[S[1, {gt3}], -U[3], U[3]] == {{(-I/4)*g2^2*vd*GaugeXi[Wm]*ZH[gt3, 1] - (I/4)*g2^2*vu*GaugeXi[Wm]*ZH[gt3, 2]}},
 C[-S[3, {gt3}], -U[2], U[3]] == {{(I/4)*CTW*g2^2*vd*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*g1*g2*STW*vd*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*CTW*g2^2*vu*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*g1*g2*STW*vu*GaugeXi[Z]*ZP[gt3, 2]}},
 C[S[1, {gt3}], -U[4], U[4]] == {{(-I/4)*g2^2*vd*GaugeXi[Wm]*ZH[gt3, 1] - (I/4)*g2^2*vu*GaugeXi[Wm]*ZH[gt3, 2]}},
 C[S[3, {gt3}], -U[2], U[4]] == {{(I/4)*CTW*g2^2*vd*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*g1*g2*STW*vd*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*CTW*g2^2*vu*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*g1*g2*STW*vu*GaugeXi[Z]*ZP[gt3, 2]}},
 C[S[1, {gt3}], -U[2], U[2]] == {{(-I/8)*g1^2*vd*GaugeXi[Z]*ZH[gt3, 1] + (I/8)*CTW^2*g1^2*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*g2^2*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*CTW^2*g2^2*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/2)*CTW*g1*g2*STW*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*g1^2*STW^2*vd*GaugeXi[Z]*ZH[gt3, 1] + (I/8)*g2^2*STW^2*vd*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*g1^2*vu*GaugeXi[Z]*ZH[gt3, 2] + (I/8)*CTW^2*g1^2*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*g2^2*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*CTW^2*g2^2*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/2)*CTW*g1*g2*STW*vu*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*g1^2*STW^2*vu*GaugeXi[Z]*ZH[gt3, 2] + (I/8)*g2^2*STW^2*vu*GaugeXi[Z]*ZH[gt3, 2]}},
 C[S[3, {gt3}], -U[3], U[2]] == {{(-I/4)*CTW*g2^2*vd*GaugeXi[Wm]*ZP[gt3, 1] + (I/4)*g1*g2*STW*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*CTW*g2^2*vu*GaugeXi[Wm]*ZP[gt3, 2] + (I/4)*g1*g2*STW*vu*GaugeXi[Wm]*ZP[gt3, 2]}},
 C[-S[3, {gt3}], -U[4], U[2]] == {{(-I/4)*CTW*g2^2*vd*GaugeXi[Wm]*ZP[gt3, 1] + (I/4)*g1*g2*STW*vd*GaugeXi[Wm]*ZP[gt3, 1] - (I/4)*CTW*g2^2*vu*GaugeXi[Wm]*ZP[gt3, 2] + (I/4)*g1*g2*STW*vu*GaugeXi[Wm]*ZP[gt3, 2]}},
 C[-U[5, {ct1}], U[5, {ct2}], V[5, {ct3}]] == {{g3*fSU3[ct1, ct2, ct3]}, {0}},
 C[-U[3], U[1], V[3]] == {{I*g2*STW}, {0}},
 C[-U[4], U[1], -V[3]] == {{(-I)*g2*STW}, {0}},
 C[-U[3], U[3], V[1]] == {{(-I)*g2*STW}, {0}},
 C[-U[3], U[3], V[2]] == {{(-I)*CTW*g2}, {0}},
 C[-U[1], U[3], -V[3]] == {{I*g2*STW}, {0}},
 C[-U[2], U[3], -V[3]] == {{I*CTW*g2}, {0}},
 C[-U[4], U[4], V[1]] == {{I*g2*STW}, {0}},
 C[-U[1], U[4], V[3]] == {{(-I)*g2*STW}, {0}},
 C[-U[2], U[4], V[3]] == {{(-I)*CTW*g2}, {0}},
 C[-U[4], U[4], V[2]] == {{I*CTW*g2}, {0}},
 C[-U[3], U[2], V[3]] == {{I*CTW*g2}, {0}},
 C[-U[4], U[2], -V[3]] == {{(-I)*CTW*g2}, {0}}
 }

 
Conjugate[g1] ^= g1; 
Conjugate[g2] ^= g2; 
Conjugate[g3] ^= g3; 
Conjugate[M12] ^= M12; 
Conjugate[vS] ^= vS; 
Conjugate[ZZ[a___]] ^= ZZ[a]; 
Conjugate[ZH[a___]] ^= ZH[a]; 
Conjugate[ZA[a___]] ^= ZA[a]; 
Conjugate[ZP[a___]] ^= ZP[a]; 
Conjugate[aEWinv] ^= aEWinv; 
Conjugate[v] ^= v; 
Conjugate[Gf] ^= Gf; 
