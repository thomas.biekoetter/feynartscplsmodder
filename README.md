# feynartscplsmodder

Introduces abbreviations to potentially
large coupling expressions in a FeynArts
modelfile that was created with the
help of SARAH. This has to main
advantages:

1. FormCalc takes a long time to simplify large expressions, or it might not terminate at all if it runs out of memory. The abbreviations shorten the expressions and prevent FormCalc from trying to simplify the expressions.
2. For the numerical evaluation of the amplitudes at loop level, it is more efficient to compute the values of the tree-level couplings once in the beginning, instead of re-computing the couplings multiple times within a large expression of an amplitude in which the couplings appear repeatedly.

## Usage

The bash script calls a Mathematica script
and two python scripts. Both should therefore
be installed on your computer.
Copy the FeynArts modelfile that you want
to abbreviate in the folder of the
repository and then type:
```
MF=modelfile.mod bash run.sh
```
The envirounment variable MF should be
set to the name of the FeynArts modelfile.
This will create a file Abbrmodelfile.mod in which
the couplings are abbreviated. If you use this
modelfile in a FeynArts session, the explicit form
of the couplings can be inserted by doing:
```
amp/.cplreplacements
```

### Example

To run the example, you should type:
```
MF=STHDMIIEWSB.mod bash run.sh
```
This will create the modelfile AbbrSTHDMIIEWSB.mod.
