Get["FAfilename.m"];
Get[modelfile];
Mcpls = M$CouplingMatrices;
Mcplsnew = Mcpls;
cplreplacements = {};

Do[

  (* Get argument list *)
  particlelist = Mcpls[[icpls, 1]];
  gtlist = {};
  Do[
   particle = Abs[particlelist[[iargs]]] /. Abs -> Identity;
   If[Length[particle] == 2,
    AppendTo[gtlist, particle[[2, 1]]];
    ];
   , {iargs, Length[particlelist]}];
  gtlistunder =
   Table[gtlist[[i]] /. gtlist[[i]] -> gtlist[[i]] _, {i, 
     Length[gtlist]}];

  (* Shorten scalar couplings and save replacements *)
  onlyscalars =
   FreeQ[Mcpls[[icpls, 1]], V] && FreeQ[Mcpls[[icpls, 1]], F] && 
    FreeQ[Mcpls[[icpls, 1]], U];
  If[onlyscalars,
   scalarcpl = Mcpls[[icpls, 2, 1, 1]];
   Mcplsnew[[icpls, 2, 1, 1]] =
    Apply[defscalarcpl, Join[{icpls}, gtlist]];
   AppendTo[
    cplreplacements,
     Apply[defscalarcpl, Join[{icpls}, gtlistunder]] -> scalarcpl
    ];
   ];

  (* Shorten scalar-fermion couplings and save replacements *)
  fermscalar =
   FreeQ[Mcpls[[icpls, 1]], V] &&
    FreeQ[Mcpls[[icpls, 1]], U] && (! onlyscalars);
  If[fermscalar,
   If[FreeQ[Mcpls[[icpls, 2, 1, 1]], IndexDelta[ct1, ct2]],
    fermscalarcpl[1] = Mcpls[[icpls, 2, 1, 1]];
    Mcplsnew[[icpls, 2, 1, 1]] =
     Apply[deffermscalarcpls, Join[{icpls, 1}, gtlist]];,
    fermscalarcpl[1] =
     Coefficient[Mcpls[[icpls, 2, 1, 1]], IndexDelta[ct1, ct2]];
    Mcplsnew[[icpls, 2, 1, 1]] =
     IndexDelta[ct1, ct2] Apply[deffermscalarcpls, Join[{icpls, 1}, gtlist]];
    ];
   If[FreeQ[Mcpls[[icpls, 2, 2, 1]], IndexDelta[ct1, ct2]],
    fermscalarcpl[2] = Mcpls[[icpls, 2, 2, 1]];
    Mcplsnew[[icpls, 2, 2, 1]] =
     Apply[deffermscalarcpls, Join[{icpls, 2}, gtlist]];,
    fermscalarcpl[2] =
     Coefficient[Mcpls[[icpls, 2, 2, 1]], IndexDelta[ct1, ct2]];
    Mcplsnew[[icpls, 2, 2, 1]] =
     IndexDelta[ct1, ct2] Apply[deffermscalarcpls, Join[{icpls, 2}, gtlist]];
    ];
   AppendTo[cplreplacements,
    Apply[deffermscalarcpls, Join[{icpls, 1}, gtlistunder]] -> 
     fermscalarcpl[1]];
   AppendTo[cplreplacements,
    Apply[deffermscalarcpls, Join[{icpls, 2}, gtlistunder]] -> 
     fermscalarcpl[2]];
   ];

  (* Shorten scalar-vector couplings and save replacements *)
  vecscalar =
   FreeQ[Mcpls[[icpls, 1]], F] &&
    FreeQ[Mcpls[[icpls, 1]],
     U] && (! onlyscalars) && (! FreeQ[Mcpls[[icpls, 1]], S]);
  If[vecscalar,
   vecscalarcpl = Mcpls[[icpls, 2, 1, 1]];
   Mcplsnew[[icpls, 2, 1, 1]] =
    Apply[defvecscalarcpl, Join[{icpls}, gtlist]];
   AppendTo[cplreplacements,
    Apply[defvecscalarcpl, Join[{icpls}, gtlistunder]] -> vecscalarcpl];
   ];
  , {icpls, Length[Mcpls]}];

(* Save everything in temporary .m file *)
M$CouplingMatrices = Mcplsnew;
filename = "Abbr" <> modelfile <> ".m";
Save[filename, a] (* Just to avoid warning of DeleteFile if file not present *)
DeleteFile[filename];
Save[filename, M$CouplingMatrices];
Save[filename, cplreplacements];

Quit[]
