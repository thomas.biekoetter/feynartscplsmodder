def main():

    # Sort out filenames
    with open("FAfilename.m", "r") as f:
        lns = f.readlines()
    ln = lns[0]
    nameold = ln.split("\"")[1]
    namenew = "Abbr" + nameold

    # Read old modelfile
    with open(nameold, "r") as f:
        lnsold = f.readlines()

    # Read temporary file
    with open("Abbr" + nameold + ".m", "r") as f:
        lnstem = f.readlines()

    # Construct new file
    # Save part of modelfile above coupling matrix
    lnsnew = []
    for ln in lnsold:
        if not "CouplingMatrices" in ln:
            lnsnew.append(ln)
        else:
            break

    # Add coupling matrix from temp file
    for i, ln in enumerate(lnstem):
        if ln == " \n":
            iendcplmatrix = i
            break
    try:
        cplmatrix = lnstem[0:iendcplmatrix]
    except UnboundLocalError:
        iendcplmatrix = len(lnstem)
        cplmatrix = lnstem[0:iendcplmatrix]
    lnsnew += cplmatrix
    lnsnew += ["\n"]

    # Add coupling replacements from temp file
    istartrplc = -1
    for i, ln in enumerate(lnstem):
        if "cplreplacements" in ln:
            istartrplc = i
        if (istartrplc > 0) and (ln == " \n"):
            iendrplc = i
            break
    try:
        rplcmatrix = lnstem[istartrplc:iendrplc]
        lnsnew += rplcmatrix
    except UnboundLocalError:
        pass
    lnsnew += ["\n"]

    # Add part below coupling matrix from old modelfile
    i = 0
    for ln in reversed(lnsold):
        if ln == " \n":
            irest = len(lnsold) - i - 1
            break
        i += 1
    try:
        lnsnew += lnsold[irest:-1]
    except UnboundLocalError:
        pass

    # Write new lines to new model file
    with open(namenew, "w") as f:
        for ln in lnsnew:
            f.write(ln)

main()
