import sys


def main(argv):

    try:
        name = argv[0]
    except:
        with open("FAfilename.m", "r") as f:
            lns = f.readlines()
        ln = lns[0]
        name = ln.split("\"")[1]
        name = "Abbr" + name + ".m"

    with open(name, "r") as f:
        lns = f.readlines()

    havepassedcpl = False

    new = []
    for ln in lns:
        x = ln
        x = x.replace("gt1*_", "gt1_")
        x = x.replace("gt2*_", "gt2_")
        x = x.replace("gt3*_", "gt3_")
        x = x.replace("gt4*_", "gt4_")
        x = x.replace("e1x1*_", "e1x1_")
        x = x.replace("e1x1*_", "e1x1_")
        x = x.replace("e1x2*_", "e1x2_")
        x = x.replace("e2x1*_", "e2x1_")
        x = x.replace("e2x2*_", "e2x2_")
        if not havepassedcpl:
            if "cplreplacements" in x:
                havepassedcpl = True
            x = x.replace("->", ":>")
        else:
            x = x.replace("->", ":>")
        new.append(x)

    with open(name, "w") as f:
        for ln in new:
            f.write(ln)

if __name__ == "__main__":
    main(sys.argv[1:])
